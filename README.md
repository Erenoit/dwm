# Erenoit's dwm
This is my fork of Suckles's dwm. Original git repo is [here](https://git.suckless.org/dwm/).

## What is dwm?
dwm (dynamic window manager) is an extremely fast, small, and dynamic window manager for X.


## Requirements
In order to build dwm you need the Xlib header files.

### Fonts
- [Iosevka](https://github.com/be5invis/iosevka)
- [Sarasa](https://github.com/be5invis/Sarasa-Gothic)
- [JoyPixels](https://www.joypixels.com/)

## Installation
Edit config.mk to match your local setup (dwm is installed into the /usr/local namespace by default).

Afterwards enter the following command to build and install dwm (if necessary as root):

```shell
$ make clean install
```

It will also create `dwm.desktop` file for display managers.

## Configuration
The configuration of dwm is done by creating a custom config.h and (re)compiling the source code.

## Patches
All the patches are included in `patches/` folder.
- [actualfullscreen](https://dwm.suckless.org/patches/actualfullscreen/)
- [attachbottom](https://dwm.suckless.org/patches/attachbottom/)
- [autostart](https://dwm.suckless.org/patches/autostart/) **Mostly Rewritten**
- [colorbar](https://dwm.suckless.org/patches/colorbar/) **Extended**
- [decoration hints](https://dwm.suckless.org/patches/decoration_hints/)
- [fullgaps](https://dwm.suckless.org/patches/fullgaps/) **toggle version**
- [movestack](https://dwm.suckless.org/patches/movestack/)
- [pertag](https://dwm.suckless.org/patches/pertag/)
