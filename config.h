// vim:fileencoding=utf-8:foldmethod=marker
/* See LICENSE file for copyright and license details. */

// INIT {{{
#define STATUSBAR "dwmblocks"
static const char *autostart_path = ".scripts/autostart";
// }}}

// APPEARANCE {{{
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const Gap default_gap        = {.isgap = 1, .realgap = 10, .gappx = 10};
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int underline          = 1;        /* 1 means draw underline on sel and warn tags */
static const int underline_tickness = 1;        /* thickness of the underline */
static const char *fonts[]          = { 
    "Iosevka Nerd Font:regular:size=10:antialias=true:hinting=true",
    "Sarasa UI J:regular:size=10:antialias=true:hinting=true",
    "JoyPixels:regular:size=10:antialias=true:hinting=true",
};
static const char dmenufont[]       =   "Iosevka Nerd Font Mono:regular:size=10:antialias=true:hinting=true";
static const char *colors[][3]      = {
	/*                        fg         bg   */
	[SchemeStatus]    = { "#c0caf5", "#1a1b26" }, // Statusbar right            {text, background}
	[SchemeTagsSel]   = { "#f7768e", "#1a1b26" }, // Tagbar left selected       {text, background}
    [SchemeTagsNorm]  = { "#c0caf5", "#1a1b26" }, // Tagbar left unselected     {text, background}
    [SchemeTagsWarn]  = { "#ffaf00", "#1a1b26" }, // Tagbar left unselected     {text, background}
    [SchemeTagsEmpty] = { "#313f55", "#1a1b26" }, // Tagbar left unselected     {text, background}
    [SchemeInfoSel]   = { "#87abf7", "#1a1b26" }, // infobar middle  selected   {text, background}
    [SchemeInfoNorm]  = { "#313f55", "#1a1b26" }, // infobar middle  unselected {text, background}
    [SchemeBorder]    = { "#004cff", "#5cb2ff" }, // border color               {active, inactive}
};
// }}}

// COMMANDS {{{
static const char *dmenucmd[] = { "dmenu_run", "-fn", dmenufont, "-nb", "#222222", "-nf", "#bbbbbb", "-sb", "#005577", "-sf", "#eeeeee", NULL };
static const char *termcmd[]  = { "st", NULL };

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }
// }}}

// TAGS {{{
static const char *tags[] = { " ", " ", " ", "󰙯 ", " ", " ", " ", " ", " ", "󰄶 " };

static const Rule rules[] = {
	/* class              instance     title      tags mask   isfloating   monitor */

    /* --------------------------- Workspace Assigments -------------------------- */
    // 1st Workspace
	{ "Alacritty",          NULL,       NULL,       1 << 0,       0,         -1 },
	{ "alacritty",          NULL,       NULL,       1 << 0,       0,         -1 },
	{ "Kitty",              NULL,       NULL,       1 << 0,       0,         -1 },
	{ "kitty",              NULL,       NULL,       1 << 0,       0,         -1 },
	{ "Termite",            NULL,       NULL,       1 << 0,       0,         -1 },
	{ "termite",            NULL,       NULL,       1 << 0,       0,         -1 },
	{ "St-256color",        NULL,       NULL,       1 << 0,       0,         -1 },
	{ "st-256color",        NULL,       NULL,       1 << 0,       0,         -1 },
	{ "unity",              NULL,       NULL,       1 << 0,       0,         -1 },

    // 2nd Workspace
	{ "Firefox",            NULL,       NULL,       1 << 1,       0,         -1 },
	{ "firefox",            NULL,       NULL,       1 << 1,       0,         -1 },
	{ "Tor Browser",        NULL,       NULL,       1 << 1,       0,         -1 },
	{ "tor browser",        NULL,       NULL,       1 << 1,       0,         -1 },
	{ "QuteBrowser",        NULL,       NULL,       1 << 1,       0,         -1 },
	{ "qutebrowser",        NULL,       NULL,       1 << 1,       0,         -1 },
	{ "Navigator",          NULL,       NULL,       1 << 1,       0,         -1 },
	{ "navigator",          NULL,       NULL,       1 << 1,       0,         -1 },
	{ "Chromium",           NULL,       NULL,       1 << 1,       0,         -1 },
	{ "chromium",           NULL,       NULL,       1 << 1,       0,         -1 },
	{ "Brave",              NULL,       NULL,       1 << 1,       0,         -1 },
	{ "brave",              NULL,       NULL,       1 << 1,       0,         -1 },
	{ "Transmission",       NULL,       NULL,       1 << 1,       0,         -1 },
	{ "transmission",       NULL,       NULL,       1 << 1,       0,         -1 },

    // 3rd Workspace
	{ "Steam",              NULL,       NULL,       1 << 2,       1,         -1 },
	{ "steam",              NULL,       NULL,       1 << 2,       1,         -1 },
	{ NULL,                 NULL,       "Steam",    1 << 2,       1,         -1 },
	{ NULL,                 NULL,       "steam",    1 << 2,       1,         -1 },
	{ "Lutris",             NULL,       NULL,       1 << 2,       0,         -1 },
	{ "lutris",             NULL,       NULL,       1 << 2,       0,         -1 },
	{ "Wine",               NULL,       NULL,       1 << 2,       0,         -1 },
	{ "wine",               NULL,       NULL,       1 << 2,       0,         -1 },
	{ "Proton",             NULL,       NULL,       1 << 2,       0,         -1 },
	{ "proton",             NULL,       NULL,       1 << 2,       0,         -1 },

    // 4th Workspace
	{ "Discord",            NULL,       NULL,       1 << 3,       0,         -1 },
	{ "discord",            NULL,       NULL,       1 << 3,       0,         -1 },
	{ "TelegramDesktop",    NULL,       NULL,       1 << 3,       0,         -1 },
	{ "telegramDesktop",    NULL,       NULL,       1 << 3,       0,         -1 },
	{ "Zoom",               NULL,       NULL,       1 << 3,       1,         -1 },
	{ "zoom",               NULL,       NULL,       1 << 3,       1,         -1 },

    // 5th Workspace
	{ "Spotify",            NULL,       NULL,       1 << 4,       0,         -1 },
	{ "spotify",            NULL,       NULL,       1 << 4,       0,         -1 },
	{ NULL,                 "Spotify",  NULL,       1 << 4,       0,         -1 },
	{ NULL,                 "spotify",  NULL,       1 << 4,       0,         -1 },
	{ NULL,                 NULL,       "Spotify",  1 << 4,       0,         -1 },
	{ NULL,                 NULL,       "spotify",  1 << 4,       0,         -1 },
	{ "Clementine",         NULL,       NULL,       1 << 4,       0,         -1 },
	{ "clementine",         NULL,       NULL,       1 << 4,       0,         -1 },
	{ "Vlc",                NULL,       NULL,       1 << 4,       0,         -1 },
	{ "vlc",                NULL,       NULL,       1 << 4,       0,         -1 },
	{ "Mpv",                NULL,       NULL,       1 << 4,       0,         -1 },
	{ "mpv",                NULL,       NULL,       1 << 4,       0,         -1 },

    // 6th Workspace
	{ "Pcmanfm",            NULL,       NULL,       1 << 5,       0,         -1 },
	{ "pcmanfm",            NULL,       NULL,       1 << 5,       0,         -1 },
	{ "Pcmanfm-qt",         NULL,       NULL,       1 << 5,       0,         -1 },
	{ "pcmanfm-qt",         NULL,       NULL,       1 << 5,       0,         -1 },
	{ "Nautilus",           NULL,       NULL,       1 << 5,       0,         -1 },
	{ "nautilus",           NULL,       NULL,       1 << 5,       0,         -1 },
	{ "Nemo",               NULL,       NULL,       1 << 5,       0,         -1 },
	{ "nemo",               NULL,       NULL,       1 << 5,       0,         -1 },
	{ "Thunar",             NULL,       NULL,       1 << 5,       0,         -1 },
	{ "thunar",             NULL,       NULL,       1 << 5,       0,         -1 },
	{ "Dolphin",            NULL,       NULL,       1 << 5,       0,         -1 },
	{ "dolphin",            NULL,       NULL,       1 << 5,       0,         -1 },

    // 7th Workspace
	{ "Mailspring",         NULL,       NULL,       1 << 6,       0,         -1 },
	{ "mailspring",         NULL,       NULL,       1 << 6,       0,         -1 },
	{ "Thunderbird",        NULL,       NULL,       1 << 6,       0,         -1 },
	{ "Thunderbird",        NULL,       NULL,       1 << 6,       0,         -1 },
	{ "Mail",               NULL,       NULL,       1 << 6,       0,         -1 },
	{ "mail",               NULL,       NULL,       1 << 6,       0,         -1 },
	{ "Evolution",          NULL,       NULL,       1 << 6,       0,         -1 },
	{ "evolution",          NULL,       NULL,       1 << 6,       0,         -1 },

    // 8th Workspace
	{ "Feh",                NULL,       NULL,       1 << 7,       0,         -1 },
	{ "feh",                NULL,       NULL,       1 << 7,       0,         -1 },
	{ "Nitrogen",           NULL,       NULL,       1 << 7,       0,         -1 },
	{ "nitrogen",           NULL,       NULL,       1 << 7,       0,         -1 },
	{ "Gimp",               NULL,       NULL,       1 << 7,       1,         -1 },
	{ "gimp",               NULL,       NULL,       1 << 7,       1,         -1 },
	{ "Inkspace",           NULL,       NULL,       1 << 7,       0,         -1 },
	{ "inkspace",           NULL,       NULL,       1 << 7,       0,         -1 },
	{ "Nomacs",             NULL,       NULL,       1 << 7,       0,         -1 },
	{ "nomacs",             NULL,       NULL,       1 << 7,       0,         -1 },

    // 9th Workspace
	{ "OBS",                NULL,       NULL,       1 << 8,       0,         -1 },
	{ "Obs",                NULL,       NULL,       1 << 8,       0,         -1 },
	{ "obs",                NULL,       NULL,       1 << 8,       0,         -1 },
	{ "Audacity",           NULL,       NULL,       1 << 8,       0,         -1 },
	{ "audacity",           NULL,       NULL,       1 << 8,       0,         -1 },

    // 10th Workspace
	{ "Virtualbox Manager", NULL,       NULL,       1 << 9,      0,         -1 },
	{ "virtualbox manager", NULL,       NULL,       1 << 9,      0,         -1 },
	{ "Virtualbox Machine", NULL,       NULL,       1 << 9,      0,         -1 },
	{ "virtualbox machine", NULL,       NULL,       1 << 9,      0,         -1 },
	{ "Vmplayer",           NULL,       NULL,       1 << 9,      0,         -1 },
	{ "vmplayer",           NULL,       NULL,       1 << 9,      0,         -1 },
    
    /* ------------------------ Floating Window Assigments ----------------------- */
    { "MEGA",               NULL,       NULL,       0,           1,         -1 },
    { "Mega",               NULL,       NULL,       0,           1,         -1 },
    { "mega",               NULL,       NULL,       0,           1,         -1 },
};
// }}}

// LAYOUTS {{{
static const float mfact          = 0.55; /* factor of master area size [0.05..0.95] */
static const int   nmaster        = 1;    /* number of clients in master area */
static const int   resizehints    = 1;    /* 1 means respect size hints in tiled resizals */
static const int   lockfullscreen = 1;    /* 1 will force focus on the fullscreen window */
static const int   decorhints     = 1;    /* 1 means respect decoration hints */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};
// }}}

// KEYS {{{
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

static const Key keys[] = {
	/* modifier                     key             function        argument */

    // General
	{ MODKEY,                       XK_b,           togglebar,      {0} },
	{ MODKEY,                       XK_j,           focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,           focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_h,           setmfact,       {.f = -0.01} },
	{ MODKEY,                       XK_l,           setmfact,       {.f = +0.01} },
	{ MODKEY|ShiftMask,             XK_j,           movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,           movestack,      {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_h,           incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_l,           incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_space,       zoom,           {0} },
	{ MODKEY,                       XK_Tab,         view,           {0} },
	{ MODKEY,                       XK_q,           killclient,     {0} },

    //	Change Layout
//	{ MODKEY,                       XK_t,           setlayout,      {.v = &layouts[0]} },
//	{ MODKEY,                       XK_f,           setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_f,           togglefullscr,  {0} },
//	{ MODKEY,                       XK_f,           setlayout,      {.v = &layouts[2]} },
//	{ MODKEY,                       XK_space,       setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_f,           togglefloating, {0} },

    // Monitor
	{ MODKEY,                       XK_comma,       focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period,      focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,       tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period,      tagmon,         {.i = +1 } },

    // Gaps
	{ MODKEY,                       XK_KP_Subtract, setgaps,        {.i = -5 } },
	{ MODKEY,                       XK_KP_Add,      setgaps,        {.i = +5 } },
	{ MODKEY|ShiftMask,             XK_KP_Add,      setgaps,        {.i = GAP_RESET } },
	{ MODKEY|ShiftMask,             XK_KP_Subtract, setgaps,        {.i = GAP_TOGGLE} },
    
    // Effect All Tags
	{ MODKEY,                       XK_a,           view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_a,           tag,            {.ui = ~0 } },

    // Tags
	TAGKEYS(                        XK_1,                           0)
	TAGKEYS(                        XK_2,                           1)
	TAGKEYS(                        XK_3,                           2)
	TAGKEYS(                        XK_4,                           3)
	TAGKEYS(                        XK_5,                           4)
	TAGKEYS(                        XK_6,                           5)
	TAGKEYS(                        XK_7,                           6)
	TAGKEYS(                        XK_8,                           7)
	TAGKEYS(                        XK_9,                           8)
	TAGKEYS(                        XK_0,                           9)

    // Quit
	{ MODKEY|ShiftMask,             XK_q,           quit,           {0} },
};
// }}}

// MOUSE {{{
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */

    // Click Layout Symbol
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },

    // Click Window Title
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },

    // Click Window
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },

    // Click Tag Bar
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },

    // Click Statusbar
	{ ClkStatusText,        0,              Button1,        sigstatusbar,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigstatusbar,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigstatusbar,   {.i = 3} },
	{ ClkStatusText,        0,              Button4,        sigstatusbar,   {.i = 4} },
	{ ClkStatusText,        0,              Button5,        sigstatusbar,   {.i = 5} },
};
// }}}

